const https = require('https');
const Influx = require('influx');

let apiKey = process.env.AIRLY_API_KEY;
let infoPoint = process.env.INFO_POINT_AIRLY;
let database = process.env.INFLUXDB_DB_NAME;
let influxHost = process.env.INFLUXDB_HOST;

const influx = new Influx.InfluxDB({
    host: influxHost,
    database: database,
    schema: [
        {
            measurement: 'airly',
            fields: {
                date: Influx.FieldType.STRING,
                pm1: Influx.FieldType.FLOAT,
                pm25: Influx.FieldType.FLOAT,
                pm10: Influx.FieldType.FLOAT,
                temperature: Influx.FieldType.FLOAT,
                smog_level: Influx.FieldType.STRING,
                smog_description: Influx.FieldType.STRING,
                smog_advice: Influx.FieldType.STRING,
            },
            tags: ['air']
        }
    ],
});

const options = {
    hostname: 'airapi.airly.eu',
    port: 443,
    path: '/v2/measurements/installation?installationId=' + infoPoint,
    method: 'GET',
    headers: {
        'apikey': apiKey,},
};

function getData(){
    https.get(options, res => {
        res.setEncoding("utf8");
        let body = "";
        res.on("data", data => {
            body += data;
        });
        res.on("end", () => {
            body = JSON.parse(body);
            parseData(body);
        });
        res.on('error', (error) => {
            throw error
        });
    });
}

function parseData(data){
    console.log(data.current);
    sendToInflux(data);
}

function sendToInflux(data){
    influx.writePoints(
        [
            {
                measurement: "airly",
                fields: {
                    date: new Date(),
                    pm1: data.current.values[0].value,
                    pm25: data.current.values[1].value,
                    pm10: data.current.values[2].value,
                    temperature: data.current.values[5].value,
                    smog_level: data.current.indexes[0].level,
                    smog_description: data.current.indexes[0].description,
                    smog_advice: data.current.indexes[0].advice,
                }
            }
        ],
        {
            database: database,
            precision: "s"
        }
    )
        .catch(err => {
            console.error("Error writing data to Influx.");
        });
}

function main(){
    setInterval(getData,120000);
}

main();


