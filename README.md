# Airly Exporter to InfluxDB


This simple script is downloading main data from Airly API and push it to
InfluxDB.

## Quickstart

|-|

	git clone https://gitlab.com/pabi88/airlyexportertoinflux.git


ENVs:
	
	
	AIRLY_API_KEY= <INPUT your ApiKey from Airly>
    INFO_POINT_AIRLY= <this is number of the information point on Airly>
    INFLUXDB_DB_NAME= <name of DB on InfluxDB
    INFLUXDB_HOST= <host for InfluxDB>
